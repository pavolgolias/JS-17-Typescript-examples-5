"use strict";
function greet(person) {
    console.log("Hello, " + person.firstName);
    console.log("Hello", person);
}
function changeName(person) {
    person.firstName = "Anna";
}
var person = {
    firstName: "Max",
    hobbies: ["Sports", "Swimming"],
    //age: 27
    greet: function (lastName) {
        console.log("Hi, I am " + this.firstName + " " + lastName);
    }
};
//greet({firstName: "Max", age: 27}); // passing directly will not work if age is not present in interface
//greet({firstName: "Max", age: 27});
greet(person);
changeName(person);
greet(person);
person.greet("Any lastname");
var Person = /** @class */ (function () {
    function Person() {
    }
    Person.prototype.greet = function (lastName) {
        console.log("Hi, I am " + this.firstName + " " + lastName);
    };
    return Person;
}());
var myPerson = new Person();
myPerson.firstName = "Pavol";
myPerson.lastName = "Something";
greet(myPerson);
myPerson.greet(myPerson.lastName);
var myDoubleFunction;
myDoubleFunction = function (value1, value2) {
    return (value1 + value2) * 2;
};
console.log(myDoubleFunction(10, 20));
var oldPerson = {
    age: 90,
    firstName: "Max",
    greet: function (lastName) {
        console.log(lastName);
    }
};
console.log(oldPerson);
// INTERFACES are not compiled to plain JS - it is only for compilation check!
