## JS-17 (Typescript-5) - Typescript example project
This is an example Typescript project covering multiple Typescript features. Based on UDEMY course: https://www.udemy.com/understanding-typescript.

These examples are focusing on **interfaces**.

#### Used commands and libs:
* npm init
* npm install lite-server --save-dev
* tsc <filename.ts>
* tsc --init
* tsc
* tsc --outFile app.js circleMath.ts rectangleMath.ts app.ts 
* tsc app.ts --outFile app.js // if we use reference 
* tsc install systemjs --save
